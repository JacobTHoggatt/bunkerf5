#include <SPI.h>
#include <Arduino.h>
#include <TM1637Display.h>

#define CLK 25
#define DIO1 21
#define DIO2 19

TM1637Display display1(CLK, DIO1);
TM1637Display display2(CLK, DIO2);

//Knobs
volatile int count3 = 0; // universal count
volatile int count5 = 0; // universal count
volatile int count7 = 0; // universal count
int FLAG1 = 0; // interrupt status flag
int FLAG2 = 0; // interrupt status flag
int FLAG3 = 0; // interrupt status flag

int c1 = 32;
int b1 = 33;

int c2 = 26;
int b2 = 27;

int c3 = 14;
int b3 = 15;

//Display
int disp1 = 21;
int disp2 = 19;

//Misc Vars
int desired = 0;
int current = 0;
int alarmed = 0;

int lights = 16;
int siren = 4;

int pump1 = 17;
int pump2 = 5;

int pumped = 0;

int bigSwitch = 18;

void setup() {
  pinMode(c1, INPUT);
  pinMode(b1, INPUT);

  pinMode(c2, INPUT);
  pinMode(b2, INPUT);

  pinMode(c3, INPUT);
  pinMode(b3, INPUT);

  Serial.begin(9600);
  Serial.println(count3);

  attachInterrupt(c1, FLAG1, RISING);
  attachInterrupt(c2, FLAG2, RISING);
  attachInterrupt(c3, FLAG3, RISING);
  // interrupt c digital pin positive edge trigger
}

void loop() {
  if(digitalRead(bigSwitch) == false){
    started = 0;
    isEmergency();
    
  }
  
  if(digitalRead(bigSwitch)){
    started = 1;
  }
  
  if(pumped == 0){
    pumpOut();
    pumped = 1;
  }

  if(started == 1) {
    if (alarmed == 0) {
      desired = random(11, 99);
      display1.showNumberDec(desired, true);
      alarm = 1;
      current = 0;
      count3 = 0;
      count5 = 0;
      count7 = 0;
    }

    if (alarmed == 1) {
      current = (count3 * 3) + (count3 * 5) + (count3 * 7);
      display.showNumberDec(current, true);
      isEmergency();
      if (desired == current) {
        if(pumped == 1){
          pumpIn();
          pumped = 2;
          }
        }
        alarm = random(6000, 150000);
      }
    }

    if (alarmed > 1) {
      display1.setBrightness(7, false);  // Turn off
      display2.setBrightness(7, false);  // Turn off
      noEmergency();
      delay(10);
      alarm --;
    }

  }

  if (FLAG1)   {
    Serial.println(count3);
    //delay(500);
    FLAG1 = 0; // clear flag
  } // end if

  if (FLAG2)   {
    Serial.println(count5);
    //delay(500);
    FLAG2 = 0; // clear flag
  } // end if

  if (FLAG3)   {
    Serial.println(count7);
    //delay(500);
    FLAG3 = 0; // clear flag
  } // end if




} // end loop


void flag1() {
  FLAG1 = 1;
  // add 1 to count for CW
  if (digitalRead(c1) && !digitalRead(b1)) {
    count3 ++ ;
  }

  // subtract 1 from count for CCW
  if (digitalRead(c1) && digitalRead(b1)) {
    count3 -- ;


  }
}

void flag2() {
  FLAG2 = 1;
  // add 1 to count for CW
  if (digitalRead(c2) && !digitalRead(b2)) {
    count5 ++ ;
  }

  // subtract 1 from count for CCW
  if (digitalRead(c2) && digitalRead(b2)) {
    count5 -- ;


  }
}

void flag3() {
  FLAG3 = 1;
  // add 1 to count for CW
  if (digitalRead(c3) && !digitalRead(b3)) {
    count7 ++ ;
  }

  // subtract 1 from count for CCW
  if (digitalRead(c3) && digitalRead(b3)) {
    count7 -- ;


  }
}

void noEmergency(){
  digitalWrite(lights, HIGH);
}

void isEmergency(){
  digitalWrite(lights,LOW);
}

void pumpIn(){
  digitalWrite(pump1, HIGH);
  delay(60000);
  digitalWrite(pump1,LOW);
}

void pumpOut(){
  digitalWrite(pump2, HIGH);
  delay(60000);
  digitalWrite(pump2,LOW);
}
