//Jacob Hoggatt 2021

int stage = 1;
int buzz = 12;
int yLed = 27;
int gLed = 26;
int rLed = 14;
int tape = 25;
int door1 = 32;
int door2 = 33;
int inputButton = 13;
int input[] = {22, 21, 19, 18, 17, 4, 16, 2, 15};
char inputChar[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'Z'};
int select = 23;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(yLed, OUTPUT);
  pinMode(gLed, OUTPUT);
  pinMode(rLed, OUTPUT);
  pinMode(tape, OUTPUT);
  pinMode(door1, OUTPUT);
  pinMode(door2, OUTPUT);
  pinMode(select, OUTPUT);
  pinMode(buzz, OUTPUT);
  digitalWrite(select, HIGH);

  for (int i = 0; i <= 8; i++) {
    pinMode(input[i], INPUT_PULLDOWN);
  }
  delay(1000);
}

void loop() {
  Serial.println("loop");
  switch (stage) {
    case 1:
      Serial.println("Switch 1 Active");
      digitalWrite(yLed, LOW);
      digitalWrite(gLed, LOW);
      digitalWrite(rLed, LOW);
      digitalWrite(tape, LOW);
      digitalWrite(door1, LOW);
      digitalWrite(door2, LOW);
      digitalWrite(buzz, LOW);

      enterCode(1);
      break;
    case 2:
      Serial.println("Switch 2 Active");
      digitalWrite(yLed, HIGH);
      digitalWrite(gLed, LOW);
      digitalWrite(rLed, LOW);
      digitalWrite(tape, HIGH);
      digitalWrite(door1, LOW);
      digitalWrite(door2, LOW);
      digitalWrite(buzz, LOW);

      buzzerCode(1);
      for (int j = 0; j <= 99;j++){
        delaySec(.05);
        if(digitalRead(inputButton)){
          buzzBeep;
          delaySec(.1);
          buzzBeep;
          Serial.println("enter code 2 begin");
          buzzBeep;
          delaySec(.1);
          buzzBeep;
          enterCode(2);
          break;
          }
      }
      
      
      break;
    case 3:
      digitalWrite(door2, HIGH);
      buzzerCode(2);
      delaySec(5);
      break;
    case 4:

      break;
    default:

      break;
  }
  delay(1000);
}

void enterCode(int x) {
  Serial.println("EnterCode Active");
  int c1[] = {1, 2, 3, 4, 5, 6};
  int c2[] = {6, 5, 4, 3, 2, 1};
  int inputCode[] = {0, 0, 0, 0, 0, 0};
  int k = 0;

  switch (x) {
    
    case 1:
    Serial.println("Enter Code 1 Active");
      while (k <= 5) {
        delay(10);
        while (digitalRead(inputButton) == false && (k<=5)) {
          delay(10);
          
          if (digitalRead(inputButton) == true) {
            Serial.println("InputButton true");
            buzzBeep();
            for (int i = 0; i <= 8; i++) {
              delay(10);
              
              if (digitalRead(input[i]) == true) {
                inputCode[k] = i + 1;
                Serial.print("input code: ");
                Serial.print(k);
                Serial.print(" = ");
                Serial.println(i+1);
              }
            }
            k++;
          }
        }
      }

      for (int n=0;n<=5;n++) if (c1[n]!=inputCode[n]){
        delay(500);
        buzzerCode(3);
        delay(500);
        Serial.println("Enter Code Failed");
        enterCode(x);
      } else {
        delay(25);
        buzzerCode(4);
        stage = 2;
        Serial.println("Enter Code 1 done");
        return;
        
        Serial.println("after return");
        break;
        
      }

    case 2:
    Serial.println("Enter Code 2 Active");
      while (k <= 5) {
        delay(10);
        while (digitalRead(inputButton) == false && (k<=5)) {
          delay(10);
          
          if (digitalRead(inputButton) == true) {
            Serial.println("InputButton true");
            buzzBeep();
            for (int i = 0; i <= 8; i++) {
              delay(10);
              
              if (digitalRead(input[i]) == true) {
                inputCode[k] = i + 1;
                Serial.print("input code: ");
                Serial.print(k);
                Serial.print(" = ");
                Serial.println(i+1);
              }
            }
            k++;
          }
        }
      }

      for (int n=0;n<=5;n++) if (c2[n]!=inputCode[n]){
        delay(500);
        buzzerCode(3);
        delay(500);
        Serial.println("Enter Code Failed");
        enterCode(x);
      } else {
        delay(25);
        buzzerCode(4);
        stage = 3;
        Serial.println("Enter Code 2 done");
        return;
        
        Serial.println("after return");
        break;
        
      }
  }
}





void buzzerCode(int x) {
  double c1[] = {.5, 1, .5, 1, .5, 1};
  double c2[] = {.5, .5, .5, 1, 1, 1};
  double c3[] = {.1, .1, .1, .1, .1, .1};
  double c4[] = {.5, .5, .5, .5, .5, .5,};

  switch (x) {
    case 1:
    Serial.println("Buzzer 1");
      for (int i = 0; i <= 5; i++) {
        digitalWrite(buzz, HIGH);
        delaySec(c1[i]);
        digitalWrite(buzz, LOW);
        delaySec(.5);
      }
      break;
    case 2:
    Serial.println("Buzzer 2");
      for (int i = 0; i <= 5; i++) {
        digitalWrite(buzz, HIGH);
        digitalWrite(gLed,HIGH);
        delaySec(c2[i]);
        digitalWrite(buzz, LOW);
        delaySec(.5);
      }
      break;
    case 3:
    Serial.println("Buzzer 3");
     digitalWrite(rLed, HIGH);
      for (int i = 0; i <= 5; i++) {
        digitalWrite(buzz, HIGH);
        delaySec(c3[i]);
        digitalWrite(buzz, LOW);
        delaySec(.1);
        
      }
      delaySec(1);
      digitalWrite(rLed,LOW);
      break;

    case 4:
    Serial.println("Buzzer 4");
    digitalWrite(gLed, HIGH);
      for (int i = 0; i <= 5; i++) {
        digitalWrite(buzz, HIGH);
        delaySec(c4[i]);
        digitalWrite(buzz, LOW);
        delaySec(.1);
      }
      
      delaySec(.25);
      digitalWrite(gLed,LOW);
      break;
  }
  delaySec(.25);

}

void buzzBeep(){
  digitalWrite(buzz, HIGH);
  delaySec(.2);      
  digitalWrite(buzz, LOW);
        
}
void delaySec(double x) {
  delay(x * 1000);
}
