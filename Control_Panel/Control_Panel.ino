//Jacob Hoggatt 2021

//Inputs
int keypad = 32;
int switch1 = 33;
int switch2 = 25;
int switch3 = 26;
int turnkey1 = 27;
int turnkey2 = 14;
//int redButton = 13;

//Outputs
int keypadLed = 21;
int switchLed = 19;
int turnkeyLed = 18;
//int redButtonLed = 5;
int doorLock = 17;
int playAudio = 16;

//Variables
bool played = false;
bool stage1 = false;
bool stage2 = false;
bool stage3 = false;
//bool stage4 = false;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(keypad, INPUT_PULLDOWN);
  pinMode(switch1, INPUT_PULLDOWN);
  pinMode(switch2, INPUT_PULLDOWN);
  pinMode(switch3, INPUT_PULLDOWN);
  pinMode(turnkey1, INPUT_PULLDOWN);
  pinMode(turnkey2, INPUT_PULLDOWN);
  //pinMode(redButton, INPUT_PULLDOWN);
  
  pinMode(keypadLed, OUTPUT);
  pinMode(switchLed, OUTPUT);
  pinMode(turnkeyLed, OUTPUT);
  //pinMode(redButtonLed, OUTPUT);
  pinMode(doorLock, OUTPUT);
  pinMode(playAudio, OUTPUT);
}

void loop() {
  //Serial.println("loop");

  

  if(digitalRead(keypad)){
    digitalWrite(keypadLed, HIGH);
    stage1 = true;
  }else{digitalWrite(keypadLed, LOW);}
  
  if(digitalRead(switch1) && digitalRead(switch2) && digitalRead(switch3)){
    digitalWrite(switchLed, HIGH);
    stage2 = true;
  }else{digitalWrite(switchLed, LOW);}
  
  if(digitalRead(turnkey1) && digitalRead(turnkey2)){
    digitalWrite(turnkeyLed, HIGH);
    stage3 = true;
  }else{digitalWrite(turnkeyLed, LOW);}
  
  //if(digitalRead(redButton)){
   // digitalWrite(redButtonLed, HIGH);
   // stage4 = true;
 // }else{digitalWrite(redButtonLed, LOW);}

  if((stage1 == true) && (stage2 == true) && (stage3 == true) && (played == false)){
    digitalWrite(playAudio, HIGH);
    delaySec(.5);
    digitalWrite(playAudio, LOW);
    delaySec(10);
    digitalWrite(doorLock, HIGH);
    played = true;
  }
  
  digitalWrite(keypadLed, HIGH);
  digitalWrite(switchLed, HIGH);
  digitalWrite(turnkeyLed, HIGH);
  delaySec(.25);
  if(stage1 == false){digitalWrite(keypadLed, LOW);}
  if(stage2 == false)digitalWrite(switchLed, LOW);}
  if(stage3 == false)digitalWrite(turnkeyLed, LOW);}
  
  
  
  //digitalWrite(redButtonLed, HIGH);
  delaySec(.5);
}


void delaySec(double x) {
  delay(x * 1000);
}
